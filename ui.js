var predChart, learnChart;

$(() => {
    initChart();
    model = createModel();

    $('#parse').one('click', () => {
        data = new Data();
        data.parse($('#dataSel').val(), $('#trainCheck').is(":checked")).then(() => {
            log('data parsed');
            attachButtons();
        }, );
    })

    $('#predict').click(() => {
        if (model) {
            const test = drawNum(Math.floor(Math.random() * (9000)));
            const res = model.predict(test.xs);
            res.print();
            res.data().then(label => {
                drawPrediction(label);
            })
        }
    })

    function attachButtons() {
        $('#train').on('click', () => {
            if( $(this).prop('disabled')) return;
            $(this).prop('disabled', true);

            $('#cancel').one('click', () => {
                cancelTraining = true;
            }).prop('disabled', false);

            const epochs = parseInt($('#epochs').val());
            const lrate = parseFloat( $('#lrate').val());
            const batchsize = parseInt( $('#batchsize').val());

            model = createModel();
            train(model, epochs, batchsize, lrate).then(() => {
                log('training done');
                $(this).prop('disabled', false);

                $('#save').one('click', () => {
                    saveModel($('#filename').val()).then(() => {
                        log('save done');
                    });
                }).prop('disabled', false);
            });

        }).prop('disabled', false);

        $('#load').on('click', () => {
            loadModel($('#filename').val()).then((result) => {
                model = result;
                log('load done');
            });
        }).prop('disabled', false);
    }
})

async function saveModel(path) {
    if(!path) path = 'mnistModel';
    await model.save('downloads://' + path);
    const saveResult = await model.save('localstorage://' + path);
    return saveResult;
}

async function loadModel(path) {
    if(!path) path = 'mnistModel';
    const loadRes = await tf.loadModel('localstorage://' + path);
    return loadRes;
}

function drawNum(n) {
    var canvas = document.getElementById('num');
    var ctx = canvas.getContext('2d');
    const test = data.getTrainingImage(n);
    for (i = 0; i < 28; i++) {
        for (j = 0; j < 28; j++) {
            let c = 255 - (test.img[i + j * 28] * 255);
            ctx.fillStyle = `rgb(${c}, ${c}, ${c})`;
            ctx.fillRect(i * 10, j * 10, 10, 10);
        }
    }

    const xs = tf.tensor4d(test.img, [1, IMAGE_H, IMAGE_W, 1]);
    return {
        xs,
        label: test.label
    };
}

function initChart() {
    var ctx = document.getElementById('prediction').getContext('2d');
    predChart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            // labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
            labels: ["Tee", "Trouser", "Pulli", "Dress", "Coat", "Sandal", "Shirt", "Sneaker", "Bag", "Boot"],
            datasets: [{
                label: "Predictions",
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                // data: label
            }]
        },
        options: {
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    ctx = document.getElementById('learning').getContext('2d');
    learnChart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: [],
            datasets: [{
                label: "Accuracy",
                borderColor: 'rgb(23, 162, 184)',
                data: [],
                fill: false,
                pointRadius: 0
            }, {
                label: "Loss",
                borderColor: 'rgb(255, 99, 132)',
                data: [],
                fill: false,
                pointRadius: 0
            }]
        },
        options: {
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }],
                xAxes: [{
                    gridLines: {
                        display: false
                    }
                }]
            }
        }
    });
}

function drawPrediction(label) {
    predChart.data.datasets[0].data = label;
    predChart.update();
}

function plotLearn(acc, loss) {
    learnChart.data.labels.push(learnChart.data.labels.length + '');
    learnChart.data.datasets[0].data.push(acc);
    learnChart.data.datasets[1].data.push(loss);
    learnChart.update();
}

function log(str) {
    var node = document.createElement("div");
    node.appendChild(document.createTextNode(str));
    $('#myLog').prepend(node);
    console.log(str);
}
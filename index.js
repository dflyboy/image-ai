// How many examples the model should "see" before making a parameter update.
const BATCH_SIZE = 64;
// How many batches to train the model for.
const TRAIN_BATCHES = 100;
// Every TEST_ITERATION_FREQUENCY batches, test accuracy over TEST_BATCH_SIZE examples.
// Ideally, we'd compute accuracy over the whole test set, but for performance
// reasons we'll use a subset.
const TEST_BATCH_SIZE = 1000;
const TEST_ITERATION_FREQUENCY = 5;
const LEARNING_RATE = 0.2;
var data;
var model;
var cancelTraining = false;

function createModel() {
    const model = tf.sequential();

    model.add(tf.layers.conv2d({
        inputShape: [28,28,1],
        kernelSize: 3,
        filters: 64,
        strides: 1,
        activation: 'relu',
        kernelInitializer: 'VarianceScaling',
        padding: 'same'
    }));

    // model.add(tf.layers.conv2d({
    //     kernelSize: 3,
    //     filters: 64,
    //     strides: 1,
    //     activation: 'relu',
    //     kernelInitializer: 'VarianceScaling'
    // }));

    model.add(tf.layers.maxPooling2d({
        poolSize: [2,2],
        strides: [2,2]
    }));

    // model.add(tf.layers.dropout({ rate: 0.25 }));

    model.add(tf.layers.conv2d({
        kernelSize: 3,
        filters: 128,
        strides: 1,
        activation: 'relu',
        kernelInitializer: 'VarianceScaling',
        padding: 'same'
    }));

    model.add(tf.layers.conv2d({
        kernelSize: 3,
        filters: 128,
        strides: 1,
        activation: 'relu',
        kernelInitializer: 'VarianceScaling'
    }));

    model.add(tf.layers.maxPooling2d({
        poolSize: [2, 2],
        strides: [2,2]
    }));

    // model.add(tf.layers.dropout({ rate: 0.25 }));

    model.add(tf.layers.conv2d({
        kernelSize: 3,
        filters: 128,
        strides: 1,
        activation: 'relu',
        kernelInitializer: 'VarianceScaling',
        padding: 'same'
    }));

    model.add(tf.layers.conv2d({
        kernelSize: 3,
        filters: 128,
        strides: 1,
        activation: 'relu',
        kernelInitializer: 'VarianceScaling'
    }));

    model.add(tf.layers.maxPooling2d({
        poolSize: [2, 2],
        strides: [2, 2]
    }));

    // model.add(tf.layers.dropout({ rate: 0.25 }));


    model.add(tf.layers.flatten());

    // model.add(tf.layers.dense({
    //     units: 512,
    //     kernelInitializer: 'VarianceScaling',
    //     activation: 'relu'
    // }));

    // model.add(tf.layers.dropout({ rate: 0.25 }));

    model.add(tf.layers.dense({
        units: 10,
        kernelInitializer: 'VarianceScaling',
        activation: 'softmax'
    }));

    return model;
}

async function train(model, batches, batchSize, learnRate) {

    const optimizer = tf.train.rmsprop(learnRate);

    model.compile({
        optimizer: optimizer,
        loss: 'categoricalCrossentropy',
        metrics: ['accuracy'],
    });

    for (let i = 0; i < batches; i++) {
        if(cancelTraining) {
            cancelTraining = false;
            break;
        }
        const batch = data.nextTrainBatch(batchSize);

        let testBatch;
        let validationData;
        // Every few batches test the accuracy of the mode.
        if (i % TEST_ITERATION_FREQUENCY === 0) {
            testBatch = data.nextTestBatch(TEST_BATCH_SIZE);
            validationData = [
                testBatch.xs.reshape([TEST_BATCH_SIZE, 28, 28, 1]), testBatch.labels
            ];
        }

        // The entire dataset doesn't fit into memory so we call fit repeatedly
        // with batches.
        const tStart = performance.now();
        const history = await model.fit(
            batch.xs.reshape([batchSize, 28, 28, 1]),
            batch.labels, {
                batchSize: batchSize,
                validationData,
                epochs: 1
            });
        const t = performance.now() - tStart;
        const loss = history.history.loss[0];
        const accuracy = history.history.acc[0];
        plotLearn(accuracy, loss);

        log(`[${i}/${batches}] L=${loss}; A=${accuracy}; t=${t}ms`);
    }
}
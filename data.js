const NUM_CLASSES = 10;
const IMAGE_W = 28, IMAGE_H = 28;
const IMAGE_SIZE = IMAGE_H * IMAGE_W;

const TRAIN_SMPLS = 50000;

class Data {
    constructor() {
        this.trainOff = 0;
        this.testOff = 0;
    }

    async parse(dataset, training) {
        if (training) {
            this.trainImages = await Data.readImages('data/' + dataset + '/train-images-idx3-ubyte');
            this.trainLabels = await Data.readLabels('data/' + dataset + '/train-labels-idx1-ubyte');

            log('Generating training tensors');
            this.trainData = this.getTrainData();
            this.trainCount = this.trainLabels.length;
            this.trainImages = null;
            this.trainLabels = null;
        }
        this.testImages = await Data.readImages('data/' + dataset + '/t10k-images-idx3-ubyte');
        this.testLabels = await Data.readLabels('data/' + dataset + '/t10k-labels-idx1-ubyte');
        log('Generating test tensors');
        this.testData = this.getTestData();
        return 0;
    }

    load(cb, training) {
        this.loadTest(() => {
            if (training) {
                this.loadTraining(cb);
            } else {
                cb();
            }
        })
        // log(util.inspect(this.trainImages.slice(0,1)));
    }

    loadTraining(cb) {
        $.getJSON('trainImg.json', trainImages => {
            this.trainImages = trainImages;
            $.getJSON('trainLabel.json', trainLabels => {
                this.trainLabels = trainLabels;
                log('Generating training tensors');
                this.trainData = this.getTrainData();
                this.trainCount = trainLabels.length;
                this.trainImages = null;
                this.trainLabels = null;
                cb();
            });
        });
    }

    loadTest(cb) {
        $.getJSON('testImg.json', testImages => {
            this.testImages = testImages;
            $.getJSON('testLabel.json', testLabels => {
                this.testLabels = testLabels;
                log('Generating test tensors');
                this.testData = this.getTestData();
                cb();
            })
        })
    }

    getTrainData() {
        const xs = tf.tensor4d(this.trainImages, [this.trainImages.length / IMAGE_SIZE, IMAGE_H, IMAGE_W, 1]);
        const labels = tf.tensor2d(this.trainLabels);
        return {
            xs,
            labels
        };
    }

    /**
     * Get all test data as a data tensor a a labels tensor.
     *
     * @param {number} numExamples Optional number of examples to get. If not
     *     provided,
     *   all test examples will be returned.
     * @returns
     *   xs: The data tensor, of shape `[numTestExamples, 28, 28, 1]`.
     *   labels: The one-hot encoded labels tensor, of shape
     *     `[numTestExamples, 10]`.
     */
    getTestData() {
        let xs = tf.tensor4d(this.testImages, [this.testImages.length / IMAGE_SIZE, IMAGE_H, IMAGE_W, 1]);
        let labels = tf.tensor2d(this.testLabels);

        return {
            xs,
            labels
        };
    }

    nextTestBatch(num) {
        let offset = Math.floor( Math.random() * (this.testLabels.length-num));
        let xs = this.testData.xs.slice([offset, 0, 0, 0], [num, IMAGE_H, IMAGE_W, 1]);
        let labels = this.testData.labels.slice([offset, 0], [num, NUM_CLASSES]);
        return {
            xs,
            labels
        };
    }

    nextTrainBatch(num) {
        let offset = Math.floor( Math.random() * (this.trainCount-num));
        let xs = this.trainData.xs.slice([offset, 0, 0, 0], [num, IMAGE_H, IMAGE_W, 1]);
        let labels = this.trainData.labels.slice([offset, 0], [num, NUM_CLASSES]);
        return {
            xs,
            labels
        };
    }

    getTrainingImage(n) {
        const img = this.testImages.slice(n * IMAGE_SIZE, (n + 1) * IMAGE_SIZE);
        const label = this.testLabels[n];
        return {
            img,
            label
        };
    }

    static readImages(file) {
        return new Promise((resolve, reject) => {
            Data.readFile(file).then(data => {
                const num = data.getInt32(4);
                console.log(num + ' images in file');

                var offset = 16;

                // var imgdata = bops.subarray(data, offset);
                let out = [];
                for (let i = 0; i < num * IMAGE_SIZE; i++) {
                    out[i] = data.getUint8(i + offset) / 255;
                }

                return resolve(out);
            });
        });
    }
    static readLabels(file) {
        return new Promise((resolve, reject) => {
            Data.readFile(file).then(data => {
                const num = data.getInt32(4);
                console.log(num + ' labels in file');

                let labels = [];
                const offset = 8;

                for (let i = 0; i < num; i++) {
                    var label = [];
                    var res = data.getUint8(offset + i);
                    for (let i = 0; i < NUM_CLASSES; i++) {
                        label[i] = res == i ? 1 : 0;
                    }
                    labels.push(label);
                    // labels[i] = data.readUInt8(offset + i);
                }

                return resolve(labels);
            });
        });
    }
    static readFile(file) {
        return new Promise((resol, rej) => {
            jQuery.ajax({
                url: file,
                cache: true,
                xhrFields: {
                    responseType: 'blob'
                },
                success: function (blob) {
                    var reader = new FileReader();
                    reader.addEventListener("loadend", function () {
                        // var buf = bops.from(reader.result);
                        var buf = new DataView(reader.result);
                        resol(buf);
                    });
                    reader.readAsArrayBuffer(blob);
                },
                error: function (e) {
                    rej(e);
                }
            });
        });
    }
}